# What is this project
This project is aimed at helping to foster the use of 20% time at Atlassian. For now it is still highly experimental and only for Atlassian internal use.

# Local Dev

Start JIRA:

```
npm run start-jira
```

Under the hood this is running:

```
atlas-run-standalone --product jira --version 7.0.0-OD-05-005 --bundled-plugins com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0.4,com.atlassian.webhooks:atlassian-webhooks-plugin:2.0.0,com.atlassian.jwt:jwt-plugin:1.2.2,com.atlassian.upm:atlassian-universal-plugin-manager-plugin:2.19.5-D20150904T173033,com.atlassian.plugins:atlassian-connect-plugin:1.1.50 --jvmargs -Datlassian.upm.on.demand=true
```

Start add-on server:

```
npm install && npm run dev
```

This will start up shipstarter in debug mode. If you want to add breakpoints to the server-side you'l have to use node-inspector:

```
npm install -g node-inspector && node-inspector
```

This will give you a URL to visit that will give you dev tools.

Build the client:

The project uses babel and galvatron to trace and build the client-side files so that we can use ES6:

```
npm run client
```

This will also watch for changes to the src files and rebuild when appropriate.