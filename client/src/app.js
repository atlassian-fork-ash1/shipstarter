'use strict';

import skate from 'skatejs';
import './router';
import './components/banner';
import './components/content';
import './api/topics';

export default skate('shipstarter-app', {
  template () {
    this.innerHTML = `
      <div id="page">
        <section id="content" role="main">
          ${this.innerHTML}
        </section>
      </div>
    `;
  }
});
