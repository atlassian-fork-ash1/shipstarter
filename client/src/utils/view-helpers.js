export function updateViewModelWithData(view, data) {
  Object.keys(data).forEach(key => {
    view[key] = data[key]
  });
}