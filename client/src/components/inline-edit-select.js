'use strict';

import 'whatwg-fetch';
import skate from 'skatejs';

function enterEditMode(elem) {
    skate.emit(elem, 'start-editing', {
        detail: {
            element: elem
        }
    });
}
//
function save(elem) {
    skate.emit(elem, 'save', {
        detail: {
            value: elem.value
        }
    });
}

function exitEditMode(elem) {
    elem.editing = false;
    const select = elem.querySelector('.inline-multi-select');
    const newValue = Array.prototype.slice.call(AJS.$(select).auiSelect2('val'));
    elem.value = newValue;
}

skate('shipstarter-inline-edit-select', {
    events: {
        click: function () {
            if(this.editable) {
                this.editing = true;
            }
        }
    },
    properties: {
        editing: {
            attr: true,
            init: false,
            type: Boolean,
            set(value) {
                if (value) {
                    enterEditMode(this);
                } else {
                    exitEditMode(this);
                }
            }
        },
        emptyState: {
            attr: false,
            init: false,
            type: String,
            set (value) {
                const currentSelection = this.value;
                if(this.value && this.value.length === 0) {
                    this.innerHTML = value;
                }
            }
        },
        value: {
            attr: false,
            set (value) {
                const elem = this;
                if(value.length > 0) {
                    elem.innerHTML = value.join(', ');
                } else {
                    elem.innerHTML = elem.emptyState || "<small> No selection </small>";
                }
            }
        },
        editable: {
            attr: true
        }
    },
    prototype: {
        renderSelect(data) {
            const SELECT_TEMPLATE = '<form class="aui"><select class="full-width-field inline-multi-select" multiple></select></form>';
            const elem = this;
            const currentSelection = elem.textContent.split(', ');

            elem.innerHTML = SELECT_TEMPLATE;
            const selectElement = elem.querySelector('.inline-multi-select');

            const $selectElement = AJS.$(selectElement);
            let optionsMarkup = '';
            data.forEach((item) => {
                optionsMarkup += convertToSelect2Data(item);
            });
            selectElement.innerHTML = optionsMarkup;
            $selectElement.auiSelect2();
            $selectElement.auiSelect2('val', currentSelection);
            $selectElement.auiSelect2('open');
            $selectElement.on('change', (e) => {
                this.editing = false;
                save(elem);
            });
        }
    }
});

function convertToSelect2Data(item) {
    return `<option value="${item}">${item}</option>`;
}
