'use strict';

import skate from 'skatejs';

export default skate('shipstarter-banner', {
  template() {
    this.innerHTML = `
      <header class="aui-page-header">
        <div class="aui-page-header-inner">
          <div class="aui-page-header-main">
            ${this.innerHTML}
          </div>
        </div>
      </header>
    `;
  }
});
