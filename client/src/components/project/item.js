'use strict';

import '../progress-bar';
import skate from 'skatejs';
var moment = window.moment;

var projectConstructor = skate('shipstarter-project-item', {
    properties: {
        description: {
            attr: true,
            set(value) {
                this.querySelector('.shipstarter-project-body p').innerHTML = value;
            }
        },
        link: {
            attr: true,
            set(value) {
                this.querySelector('.shipstarter-project-view-button').href = value;
            }
        },
        name: {
            attr: true,
            set(value) {
                this.querySelector('h3').textContent = value;
            }
        },
        img: {
            attr: true,
            set(value) {
                this.querySelector('.shipstarter-project-background').style.backgroundImage = 'url(' + value + ')';
            }
        }
    },
    template() {
        this.innerHTML = `
      <article class="shipstarter-project">
        <header class="shipstarter-project-header">
          <div class="shipstarter-project-background"></div>
          <h3 class="shipstarter-project-title"></h3>
        </header>
        <section class="shipstarter-project-body">
          <p></p>
        </section>
        <footer class="shipstarter-project-footer">
          <a href="#" class="shipstarter-project-view-button aui-button aui-button-primary">View</a>
        </footer>
      </article>
    `;
    }
});

export default function (project) {
    project = project.data;
    var newProject = projectConstructor();

    if (!newProject.img) {
        newProject.img = 'https://unsplash.it/400/300?image=' + Math.ceil(Math.random() * 100);
    }

    newProject.link = `#/project/${project.key}`;

    for (var projectProperty in project) {
        newProject[projectProperty] = project[projectProperty];
    }

    return newProject;
}
