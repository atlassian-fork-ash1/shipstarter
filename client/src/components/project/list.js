'use strict';

import * as projectsAPI from '../../api/projects';
import * as topicsAPI from '../../api/topics';
import projectItem from './item';
import arrayOf from '../../utils/property-types/array-of';
import skate from 'skatejs';

export default skate('shipstarter-project-list', {
    events: {
        'click .shipstarter-project-filter': function (e) {
            var menu = document.getElementById(e.delegateTarget.getAttribute('aria-controls'));
            var selected = e.delegateTarget.querySelector('.shipstarter-dropdown-selected');
            menu.addEventListener('click', function handler(e) {
                selected.textContent = e.target.textContent.trim();
                this.removeEventListener('click', handler);
            });
        },
        'click shipstarter-project-item': function(e) {
            window.location.href = e.delegateTarget.link;
        }
    },
    properties: {
        emptyState: {
            attr: true
        },
        projects: {
            type: arrayOf(projectItem)
        }
    },
    prototype: {
        get list() {
            return this.querySelector('.shipstarter-project-list .wrap');
        },
        filterByTopics(topics) {
            const projectsList = this;
            if(topics) {
                projectsAPI.getProjectsByTopics(topics).then(projects => {
                    projectsList.projects = projects
                });
            } else {
                projectsAPI.getProjectsByKey().then(projects => {
                    projectsList.projects = projects;
                });
            }
        },
        /**
         * sets the projects on the view and renders them
         * @param projects array of Project entity
         */
        setProjects(projects) {
            const projectList = this;
            projectList.projects = projects;
            projectList.innerHTML = '';
            projects.forEach(projectData => {
                projectList.appendChild(new projectItem(projectData));
            });

            if(projects.length === 0) {
                projectList.innerHTML = `<div class="shipstarter-no-projects">${projectList.emptyState || 'couldn\'t find any projects'}</div>`;
            }
        }
    }
});
